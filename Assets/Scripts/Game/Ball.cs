﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    public CollisionCircle CollisionCircle { get; private set; }
    public Vector2 ballDirection;
    public float ballSpeed=3f;
    
    public int minX, minY;

    private void Awake()
    {
        minX = Random.Range(-5, 5);
        minY = Random.Range(2, 25);

        ballDirection = new Vector2(minX,minY);
    }

    private void Start()
    {
        this.CollisionCircle = new CollisionCircle(gameObject,this.transform.position, this.transform.localScale.y / 2f);
        this.transform.position +=(Vector3) ballDirection * Time.deltaTime;
    }

    private void FixedUpdate()
    {
        this.transform.position += (Vector3)ballDirection * Time.deltaTime * ballSpeed ;
        CollisionCircle.position = this.transform.position;
        ballDirection.Normalize();
    }

    public void ReflectMovement(Vector2 normal)
    {
        ballDirection = Vector3.Reflect(ballDirection,(Vector3)normal);
    }

    private void OnDrawGizmosSelected()
    {
        if (CollisionCircle != null)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(CollisionCircle.position, CollisionCircle.radius);
        }
    }

    public void BallSpeedUp()
    {
        ballSpeed = 5f;
    }

    public void BallSpeedDown()
    {
        ballSpeed = 3f;
    }

    public void BallSize()
    {
        Vector3 newScale = new Vector3(transform.localScale.x*2,transform.localScale.y*2,transform.localScale.z);
        this.transform.localScale = newScale;
        float newRadius =  this.CollisionCircle.radius*2 ;
        this.CollisionCircle.radius = newRadius;
    }

  /* public Vector2 randomVector2(float angle,float minAngle)
    {
        float random = Random.value * angle + minAngle;
        return new Vector2(Mathf.Sin(random), Mathf.Cos(random));
    }

    */
}
