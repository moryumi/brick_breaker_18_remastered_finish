﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public CollisionBox CollisionBox { get; private set; }

    private void Start()
    {
        this.CollisionBox = new CollisionBox(this.gameObject,this.transform.position,this.transform.localScale.x, this.transform.localScale.y);
    }

    public void BallTouched()
    {
        GameObject.Destroy(this.gameObject);
    }

}
