﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCircle
{
    public GameObject GameObject { get; private set; }

    public Vector2 position;
    public float radius;

    public CollisionCircle(GameObject gameObject, Vector2 position, float radius)
    {
        this.GameObject = gameObject;

        this.position = position;
        this.radius = radius;
    }
}

public class CollisionBox
{
    public GameObject GameObject { get; private set; }

    public Vector2 position;
    public float width, height;

    public Vector2 Size
    {
        get
        {
            return new Vector2(width, height);
        }
    }

    public CollisionBox(GameObject gameObject, Vector2 position, float width, float height)
    {
        this.GameObject = gameObject;

        this.position = position;
        this.width = width;
        this.height = height;
    }
}

public static class CollisionManager
{
    public static bool CheckCircleBoxCollision(CollisionCircle circle, CollisionBox box, out Vector2 normal)
    {
        float NearestX = Mathf.Clamp(circle.position.x, box.position.x - box.width / 2, box.position.x + box.width / 2);
        float NearestY = Mathf.Clamp(circle.position.y, box.position.y - box.height / 2, box.position.y + box.height / 2);

        Vector2 closestPoint = new Vector2(NearestX, NearestY);
        Vector2 dp = closestPoint - circle.position;

        normal = -dp.normalized;

        bool collided = (dp.magnitude) <= (circle.radius);

        /*if (collided)
            Debug.DrawLine((Vector3)closestPoint, (Vector3)circleCenter, Color.red);
            */

        return collided;
    }

    public static bool CheckCircleCircleCollision(CollisionCircle circleFirst, CollisionCircle circleSecond,out Vector2 normal)
    {
        // float nearX = Mathf.Clamp(circleFirst.position.x,circleSecond.position.x-circleSecond.radius, circleSecond.position.x + circleSecond.radius);
        // float nearY = Mathf.Clamp(circleFirst.position.y, circleSecond.position.y - circleSecond.radius, circleSecond.position.y + circleSecond.radius);
        //float differenceRadius = circleFirst.radius - circleSecond.radius;
        //Vector2 dp = circleSecond.position - circleFirst.position;

        float differenceX = circleFirst.position.x - circleSecond.position.x;
        float differenceY = circleFirst.position.y - circleSecond.position.y;

        float distance = Mathf.Sqrt((differenceX * differenceX) + (differenceY * differenceY));

        float radiusDistance = circleFirst.radius + circleSecond.radius;
        
        float collisionPointX = (circleFirst.position.x + circleSecond.position.x) / 2;
        float collisionPointY = (circleFirst.position.y + circleSecond.position.y) / 2;
        Vector2 collisionPoint = new Vector2(collisionPointX,collisionPointY);
        Vector2 dp = collisionPoint - circleFirst.position;

        normal = -dp.normalized;

        // bool collided = Mathf.Sqrt(differenceX) + Mathf.Sqrt(differenceY) <= Mathf.Sqrt(differenceRadius);

        bool collided = (distance) <= (radiusDistance) ;
        return collided;
    }

}
