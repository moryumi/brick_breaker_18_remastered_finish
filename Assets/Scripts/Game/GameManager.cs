﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public Paddle paddle;
    public Ball ball;
    public Level level;
    
    [Range(0f, 1f)]
    public float powerupSpawnChance = 0.3f;

    public PowerupBase[] powerupPrefabs;
    private List<PowerupBase> activePowerups;
    private int bricksCount = 0;
    private int point = 5;

    public Ball ballPrefab;
    private List<Ball> activeBalls;
   
    public GameObject gameOver;
    public GameObject levelOver;
    public TextMeshProUGUI score;
    
    public event Action OnGameFinish;
    public event Action OnLevelFinish;

    private void Start()
    {
        activePowerups = new List<PowerupBase>();
        activeBalls = new List<Ball>();
        activeBalls.Add(ball);
        float xMinPosition, xMaxPosition, yPosition;
        level.GetPaddlePositions(out xMinPosition, out xMaxPosition, out yPosition);
        paddle.Initialize(xMinPosition, xMaxPosition, yPosition);

    }

    private void FixedUpdate()
    {
        Vector2 normal;

        for (int m = 0; m < activeBalls.Count; m++)
        {
            for (int n = m + 1; n < activeBalls.Count; n++)
            {
                if (CollisionManager.CheckCircleCircleCollision(activeBalls[m].CollisionCircle, activeBalls[n].CollisionCircle,out normal))
                {
                    activeBalls[m].ReflectMovement(-normal);
                    activeBalls[n].ReflectMovement(normal);
                }
            }
        }

        for (int m = 0; m < activeBalls.Count; m++)
        {
            if (CollisionManager.CheckCircleBoxCollision(activeBalls[m].CollisionCircle, paddle.CollisionBox, out normal))
            {
                activeBalls[m].ReflectMovement(normal);
            }
        }
        
        for (int i = 0; i < level.borderCollisionBoxes.Length; i++)
        {
            for (int m = 0; m < activeBalls.Count; m++)
            {
                if (CollisionManager.CheckCircleBoxCollision(activeBalls[m].CollisionCircle, level.borderCollisionBoxes[i], out normal))
                {
                    GameObject border = level.borderCollisionBoxes[i].GameObject;
                    //Debug.Log("bordeeer"+border);
                    //Debug.Log("taaag"+border.tag);

                    if (border.CompareTag("BottomWall"))
                    {
                        FinishGame();
                    }
                    activeBalls[m].ReflectMovement(normal);
                }
            }
        }
        
        for (int i = 0; i < level.bricks.Length; i++)
        {
            Brick brick = level.bricks[i];
         
            for (int m = 0; m < activeBalls.Count; m++)
            {
                if (brick != null && CollisionManager.CheckCircleBoxCollision(activeBalls[m].CollisionCircle, brick.CollisionBox, out normal))
                {
                    activeBalls[m].ReflectMovement(normal);
                    brick.BallTouched();
                    TrySpawnPowerup(brick);
                    point += 5;
                    Score();
                    if (level.brickSayi <= 0)
                    {
                        LevelFinish();
                    }
                }
            }
        }
        
        for (int i = 0; i < activePowerups.Count; i++)
        {
            PowerupBase powerup = activePowerups[i];

            if (CollisionManager.CheckCircleBoxCollision(activePowerups[i].CollisionCircle, paddle.CollisionBox, out normal))
            {
                powerup.ApplyPowerup();
               
                GameObject.Destroy(powerup.gameObject);
                activePowerups.RemoveAt(i);
                i--;
            }
        } 
    }

    private void TrySpawnPowerup(Brick brick)
    {
        float spawnChance = UnityEngine.Random.Range(0f, 1f); 

        if (spawnChance > powerupSpawnChance)
            return;

        int powerupIndex = UnityEngine.Random.Range(0, powerupPrefabs.Length);
        PowerupBase powerupPrefab = powerupPrefabs[powerupIndex];

        GameObject powerupGameObject = GameObject.Instantiate(powerupPrefab.gameObject);
        powerupGameObject.transform.position = brick.transform.position;

        PowerupBase powerupScript = powerupGameObject.GetComponent<PowerupBase>();
        powerupScript.Initialize(this);

        activePowerups.Add(powerupScript);
       // Debug.Log(""+activePowerups);
    }
    
    public void CreateBall(int ballCount, Vector3 position)
    {
        for (int i = 0; i < ballCount; i++)
        {
           System.Random rnd = new System.Random();
           //ballPrefab.gameObject.GetComponent<MeshRenderer>().GetComponent<Material>().color= Color.HSVToRGB(rnd.Next(256), rnd.Next(256), rnd.Next(256));
           GameObject ballGameObject = GameObject.Instantiate(ballPrefab.gameObject);
           ballGameObject.transform.position = position;
           Ball ballScript = ballGameObject.GetComponent<Ball>();

           activeBalls.Add(ballScript);
        }
    }
    
    private void FinishGame()
    {
        if (OnGameFinish != null)
            OnGameFinish.Invoke();
    }

    private void LevelFinish()
    {
        if (OnLevelFinish != null)
            OnLevelFinish.Invoke();
    }

    private void Score()
    {
        score.text = "" + point;

    }
}
