﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Transform leftBorder;
    public Transform rightBorder;
    public Transform topBorder;
    public Transform bottomBorder;
    public bool isGameFinish = false;

    public float paddleOffset = 2f;
    public int brickSayi = 0;

    [System.NonSerialized]
    public Brick[] bricks;

    [System.NonSerialized]
    public CollisionBox[] borderCollisionBoxes;

    private void Awake()
    {
        borderCollisionBoxes = new CollisionBox[4];
        borderCollisionBoxes[0] = new CollisionBox(leftBorder.gameObject,leftBorder.position, leftBorder.localScale.x, leftBorder.localScale.y);
        borderCollisionBoxes[1] = new CollisionBox(rightBorder.gameObject,rightBorder.position, rightBorder.localScale.x, rightBorder.localScale.y);
        borderCollisionBoxes[2] = new CollisionBox(topBorder.gameObject,topBorder.position, topBorder.localScale.x, topBorder.localScale.y);
        borderCollisionBoxes[3] = new CollisionBox(bottomBorder.gameObject,bottomBorder.position, bottomBorder.localScale.x, bottomBorder.localScale.y);

        bricks=this.GetComponentsInChildren<Brick>();
    }

    void Start()
    {
        
    }

    void Update()
    {
        brickSayi = this.GetComponentsInChildren<Brick>().Length;
        
    }

    public void GetPaddlePositions(out float xMinPosition, out float xMaxPosition, out float yPosition)
    {
        xMinPosition = leftBorder.transform.position.x + leftBorder.transform.localScale.x / 2f;
        xMaxPosition = rightBorder.transform.position.x - rightBorder.transform.localScale.x / 2f;
        yPosition = bottomBorder.transform.position.y + bottomBorder.transform.localScale.y / 2f + paddleOffset;
    }


    private void OnDrawGizmosSelected()
    {
        if (borderCollisionBoxes != null)
        {
            Gizmos.color = Color.magenta;

            for (int i = 0; i < borderCollisionBoxes.Length; i++)
            {
                CollisionBox collisionBox = borderCollisionBoxes[i];

                Vector3 position = new Vector3(collisionBox.position.x, collisionBox.position.y, 0f);
                Vector3 size = new Vector3(collisionBox.Size.x, collisionBox.Size.y, 0f);

                Gizmos.DrawWireCube(position, size);
            }
        }
    }

}
