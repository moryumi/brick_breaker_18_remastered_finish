﻿using UnityEngine;
using ToolScripts;

public class Paddle : MonoBehaviour
{
    public bool IsInitialized { get; private set; }
    public CollisionBox CollisionBox { get; private set; }

    private float xMinPosition;
    private float xMaxPosition;

    public GameObject fire;

    private void Start()
    {
        this.CollisionBox = new CollisionBox(gameObject,this.transform.position, this.transform.localScale.x, this.transform.localScale.y);
        
        TouchManager.Instance.onTouchMoved += TouchMoved;
    }

    public void Initialize(float xMinPosition, float xMaxPosition, float yPosition)
    {
        this.xMinPosition = xMinPosition + this.transform.localScale.x / 2f;
        this.xMaxPosition = xMaxPosition - this.transform.localScale.x / 2f;

        this.transform.position = new Vector3(this.transform.position.x, yPosition, this.transform.position.z);

        IsInitialized = true;
    }

    private void Update()
    {
        if (!IsInitialized)
            return;

        this.CollisionBox.position = this.transform.position;

       /* if ()
        {

        }
        */

    }

    private void TouchMoved(TouchInput touchInput)
    {
        float newXPosition = this.transform.position.x + touchInput.DeltaWorldPosition.x;
        newXPosition = Mathf.Clamp(newXPosition, xMinPosition, xMaxPosition);

        this.transform.position = new Vector3(newXPosition, this.transform.position.y, this.transform.position.z);
    }

    private void OnDrawGizmosSelected()
    {
        if (CollisionBox != null)
        {
            Vector3 position = new Vector3(CollisionBox.position.x, CollisionBox.position.y, this.transform.position.z);
            Vector3 size = new Vector3(CollisionBox.Size.x, CollisionBox.Size.y, this.transform.localScale.z);

            Gizmos.color = Color.magenta;
            Gizmos.DrawWireCube(position, size);
        }
    }
    
    public void ChangeSizeOfPaddle()
    {
        float newWidthScale = (this.transform.localScale.x / 4) * 2;
        this.transform.localScale = new Vector3(newWidthScale,this.transform.localScale.y, this.transform.localScale.z);
        float newWidth = this.CollisionBox.width / 4;
        this.CollisionBox.width = newWidth * 2;
    }

    public void PaddleFire()
    {
        GameObject firePrefab = GameObject.Instantiate(fire.gameObject);
        fire.transform.position = this.transform.position;
    }
}
