﻿using UnityEngine;

public class BallSpawnPowerup : PowerupBase
{
    public override void ApplyPowerup()
    {
        GameManager.CreateBall(2, new Vector2(this.transform.position.x, GameManager.paddle.transform.position.y + Mathf.Abs(GameManager.paddle.transform.position.y/6)));
    }
}
