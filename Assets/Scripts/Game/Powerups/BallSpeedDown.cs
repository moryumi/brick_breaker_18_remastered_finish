﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpeedDown : PowerupBase
{
    public override void ApplyPowerup()
    {
        GameManager.ball.BallSpeedDown();
    }

}
