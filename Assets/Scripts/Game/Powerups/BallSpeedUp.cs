﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpeedUp : PowerupBase
{
    public override void ApplyPowerup()
    {
        GameManager.ball.BallSpeedUp();
    }
}
