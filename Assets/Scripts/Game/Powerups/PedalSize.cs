﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedalSize : PowerupBase
{
    public override void ApplyPowerup()
    {
        GameManager.paddle.ChangeSizeOfPaddle();
    }
}
