﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBallSize : PowerupBase
{
    public override void ApplyPowerup() {

        GameManager.ball.BallSize();
    }
}
