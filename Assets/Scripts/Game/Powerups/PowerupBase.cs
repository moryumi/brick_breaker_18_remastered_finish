﻿using UnityEngine;

public abstract class PowerupBase : MonoBehaviour
{
    protected GameManager GameManager { get; private set; }

    public CollisionCircle CollisionCircle { get; private set; }



    private void Awake()
    {
        this.CollisionCircle = new CollisionCircle(gameObject,this.transform.position, this.transform.localScale.y/2f);
       
    }

    public virtual void Initialize(GameManager gameManager)
    {
        this.GameManager = gameManager;
    }

    private void Update()
    {
        CollisionCircle.position = this.transform.position;
        //Debug.Log("" + CollisionCircle.position);

        Vector3 position = this.transform.position;
        position.y -= 3 * Time.deltaTime;
        this.transform.position = position;
    }

    public abstract void ApplyPowerup();
}
