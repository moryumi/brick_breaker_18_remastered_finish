﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class CanvasController : MonoBehaviour
{
    public GameManager GameManager;
    
    public void Awake()
    {
        this.GameManager.OnGameFinish += GameManager_OnGameFinish;
        this.GameManager.OnLevelFinish += GameManager_OnLevelFinish;
    }

    private void GameManager_OnLevelFinish()
    {
        GameManager.levelOver.SetActive(true);
        Time.timeScale = 0;
    }

    private void GameManager_OnGameFinish()
    {
        GameManager.gameOver.SetActive(true);
        Time.timeScale = 0;
    }


    public void LoadLevel()
    {
        SceneManager.LoadScene("GameScene");
        Time.timeScale = 1;
    }

}